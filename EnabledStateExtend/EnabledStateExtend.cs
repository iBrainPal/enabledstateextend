﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Design.Serialization;
using System.Reflection;


namespace ZE
{
    [ProvideProperty("EnabledState", typeof(Control))]
    public class EnabledStateExtend : Component, IExtenderProvider
    {
        public delegate void OnEnabledChanged();
        public event OnEnabledChanged EnabledChanged;

        private Hashtable _enabledState = new Hashtable();
        
        private System.ComponentModel.Container components = null;

        public EnabledStateExtend(System.ComponentModel.IContainer container)
        {
            InitializeComponent();
        }
        [DefaultValue(typeof(EnabledState), "True,True,True,True,True,True,True,True"), Description("")]
        public EnabledState GetEnabledState(Control control)
        {
            EnabledState reValue = new EnabledState();
            if (this._enabledState.Contains(control))
            {
                reValue = (EnabledState)(this._enabledState[control]);
            }

            return reValue;
        }
        public void SetEnabledState(Control control, EnabledState value)
        {
            if (this._enabledState.Contains(control))
            {
                this._enabledState[control] = value;
            }
            else
            {
                this._enabledState.Add(control, value);
            }
        }

        private EnabledState.STATE defaultEnabledState = EnabledState.STATE.None;
        [Browsable(false)]
        public EnabledState.STATE CurrentEnabledState { get { return defaultEnabledState; } }

        public void ChangeEnabled(EnabledState.STATE state)
        {
            defaultEnabledState = state;

            foreach (object obj in _enabledState.Keys)
            {
                EnabledState controlEnabledState = (EnabledState)_enabledState[obj];

                //((Control)obj).Enabled = ((controlEnabledState.States & state) == state);

                InvokeSetEnabled((Control)obj, ((controlEnabledState.States & state) == state));
            }



            if (this.EnabledChanged != null && this.EnabledChanged.GetInvocationList().Length > 0)
            {
                EnabledChanged();
            }
        }

        #region Change Enabled By Invoke

        private delegate void SetControlEnabled(Control control, bool enabled);
        private SetControlEnabled setControlEnabled;

        private void SetEnabled(Control control, bool enabled)
        {
            control.Enabled = enabled;
        }

        private void InvokeSetEnabled(Control control, bool enabled)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(setControlEnabled, control, enabled);
            }
            else
            {
                SetEnabled(control, enabled);
            }
        }

        #endregion




        public EnabledStateExtend()
        {
            InitializeComponent();
        }
        
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if(components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        #region 组件设计器生成的代码
        /**//// 
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            setControlEnabled = new SetControlEnabled(SetEnabled);
        }
        #endregion

        #region IExtenderProvider 成员
        public bool CanExtend(object extendee)
        {
            // TODO: 添加 Component1.CanExtend 实现
            if (extendee is Control && !(extendee is EnabledStateExtend) && !(extendee is Form))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

    }


}
