﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Design.Serialization;
using System.Reflection;

namespace ZE
{
    [TypeConverter(typeof(EnabledStateConverter))]
    public class EnabledState
    {
        public enum STATE { None = 0, Ready = 1, Prepare = 2, Working = 4, Finish = 8, Failure = 16, Warning = 32, Error = 64, Breakdown = 128 }

        [Description("画面初期化后是否可用")]
        [DisplayName("1.初期化后可用")]
        [Category("状态")]
        [DefaultValue(true)]
        public bool Ready { get; set; }

        [Description("准备工作进行时是否可用")]
        [DisplayName("2.准备工作时可用")]
        [Category("状态")]
        [DefaultValue(true)]
        public bool Prepare { get; set; }

        [Description("工作进行时是否可用")]
        [DisplayName("3.工作时可用")]
        [Category("状态")]
        [DefaultValue(true)]
        public bool Working { get; set; }

        [Description("工作完成后是否可用")]
        [DisplayName("4.工作完成后可用")]
        [Category("状态")]
        [DefaultValue(true)]
        public bool Finish { get; set; }

        [Description("工作失败后是否可用")]
        [DisplayName("5.工作失败后可用")]
        [Category("状态")]
        [DefaultValue(true)]
        public bool Failure { get; set; }

        [Description("工作发生警报后是否可用")]
        [DisplayName("6.工作发生警报后可用")]
        [Category("状态")]
        [DefaultValue(true)]
        public bool Warning { get; set; }

        [Description("工作发生错误后是否可用")]
        [DisplayName("7.工作发生错误后可用")]
        [Category("状态")]
        [DefaultValue(true)]
        public bool Error { get; set; }

        [Description("系统发生未知故障后是否可用")]
        [DisplayName("8.系统发生未知故障后可用")]
        [Category("状态")]
        [DefaultValue(true)]
        public bool Breakdown { get; set; }
        

        [Browsable(false)]
        public STATE States
        {
            get
            {
                STATE reValue = STATE.None;

                if (Ready)
                {
                    reValue = reValue | STATE.Ready;
                }
                if (Prepare)
                {
                    reValue = reValue | STATE.Prepare;
                }
                if (Working)
                {
                    reValue = reValue | STATE.Working;
                }
                if (Finish)
                {
                    reValue = reValue | STATE.Finish;
                }
                if (Failure)
                {
                    reValue = reValue | STATE.Failure;
                }
                if (Warning)
                {
                    reValue = reValue | STATE.Warning;
                }
                if (Error)
                {
                    reValue = reValue | STATE.Error;
                }
                if (Breakdown)
                {
                    reValue = reValue | STATE.Breakdown;
                }

                return reValue;
            }
        }

        public EnabledState()
        {
            this.Ready = true;
            this.Prepare = true;
            this.Working = true;
            this.Finish = true;
            this.Failure = true;
            this.Warning = true;
            this.Error = true;
            this.Breakdown = true;
        }

        public EnabledState(bool ready, bool prepare, bool working, bool finish, bool failure, bool warning, bool error, bool breakdown)
        {
            this.Ready = ready;
            this.Prepare = prepare;
            this.Working = working;
            this.Finish = finish;
            this.Failure = failure;
            this.Warning = warning;
            this.Error = error;
            this.Breakdown = breakdown;
        }

    }

}
