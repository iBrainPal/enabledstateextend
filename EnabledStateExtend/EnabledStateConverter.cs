﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Design.Serialization;
using System.Reflection;

namespace ZE
{
    public class EnabledStateConverter : ExpandableObjectConverter
    {
        //public EnabledStateConverter()
        //{
        //}


        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                try
                {
                    string s = value as string;

                    if (s == null) return base.ConvertFrom(context, culture, value);

                    string[] ps = s.Split(new char[] { char.Parse(",") });

                    if (ps.Length == 8)
                    {
                        return new EnabledState(ps[0].Equals("True"), 
                            ps[1].Equals("True"), 
                            ps[2].Equals("True"), 
                            ps[3].Equals("True"), 
                            ps[4].Equals("True"), 
                            ps[5].Equals("True"), 
                            ps[6].Equals("True"), 
                            ps[7].Equals("True"));
                    }
                }
                catch { }
                throw new ArgumentException("Cannot convert '" + (string)value + "' to type EnabledState");
            }
            return base.ConvertFrom(context, culture, value);
        }

        //public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        //{
        //    if (destinationType == typeof(string))
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //    //return base.CanConvertTo(context, destinationType);
        //}



        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            //if ((destinationType == typeof(string)) && (value is EnabledState))
            if (destinationType == typeof(string))
            {
                EnabledState e = (EnabledState)value;
                return string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", 
                    e.Ready.ToString(), 
                    e.Prepare.ToString(),
                    e.Working.ToString(),
                    e.Finish.ToString(),
                    e.Failure.ToString(),
                    e.Warning.ToString(),
                    e.Error.ToString(),
                    e.Breakdown.ToString()
                    );
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        //public override object CreateInstance(ITypeDescriptorContext context, IDictionary propertyValues)
        //{
        //    bool ready = propertyValues["Ready"].ToString().Equals("True");
        //    bool searching = propertyValues["Searching"].ToString().Equals("True");

        //    return new EnabledState(ready, searching);
        //}

        //public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        //{
        //    if (value is EnabledState)
        //    {
        //        return TypeDescriptor.GetProperties(value, attributes);
        //    }

        //    return base.GetProperties(context, value, attributes);
        //}

        //public override bool GetCreateInstanceSupported(ITypeDescriptorContext context)
        //{
        //    //return base.GetCreateInstanceSupported(context);
        //    return true;
        //}

        //public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        //{
        //    //return base.GetPropertiesSupported(context);
        //    return true;
        //}
    }
}
